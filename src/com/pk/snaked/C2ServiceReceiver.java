package com.pk.snaked;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by pk on 6/11/14.
 */
public class C2ServiceReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        PhoneInfo phoneInfo = new PhoneInfo(context); // Initialize the device information
        Intent service = new Intent(context, C2Service.class);
        context.startService(service);
    }
}
